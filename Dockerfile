FROM ubuntu:16.04

RUN apt-get update
RUN apt-get -y install curl
RUN curl http://cumulusclips.org/cumulusclips.tar.gz > cc.tar.gz
RUN apt-get -y install apache2
RUN apt-get -y install php7.0-mysql libapache2-mod-php7.0 php7.0-mysql
RUN apt-get -y install php7.0-zip php7.0-gd php7.0-curl php7.0-xml

RUN sed -i -- 's/short_open_tag = Off/short_open_tag = On/g' /etc/php/7.0/apache2/php.ini 
RUN cat /etc/php/7.0/apache2/php.ini | grep short_open_tag

RUN rm -rf /var/www/html/*
RUN tar xvfz cc.tar.gz -C /var/www/html

CMD apache2ctl -D FOREGROUND
